﻿// Copyright (c) Argo Zhang (argo@163.com). All rights reserved.
// Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// Website: https://www.blazor.zone or https://argozhang.github.io/

using BootstrapBlazor.Components;
using BootstrapBlazorApp.Server.Data;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Localization;
using System.Diagnostics.CodeAnalysis;
using System.Reflection;

namespace BootstrapBlazorApp.Server.Components.Pages
{
    public partial class Index
    {
        [Inject]
        [NotNull]
        private IStringLocalizer<Foo>? Localizer { get; set; }

        [NotNull]
        private List<Foo>? Items { get; set; }

        private static IEnumerable<int> PageItemsSource => [10, 20, 50];

        private Client? _selectClient;

        private Task<QueryData<Foo>> OnQueryAsync(QueryPageOptions options)
        {
            if (Items == null)
            {
                Items = Foo.GenerateFoo(Localizer);
            }

            var items = Items.Where(options.ToFilterFunc<Foo>());

            // 排序
            var isSorted = false;
            if (!string.IsNullOrEmpty(options.SortName))
            {
                items = items.Sort(options.SortName, options.SortOrder);
                isSorted = true;
            }

            var total = items.Count();

            return Task.FromResult(new QueryData<Foo>()
            {
                Items = items.Skip((options.PageIndex - 1) * options.PageItems).Take(options.PageItems).ToList(),
                TotalCount = total,
                IsFiltered = true,
                IsSorted = isSorted,
                IsSearch = true
            });
        }

        private static string? GetClientNameCallback(Client dto)
        {
            return dto.ClientName;
        }

        private Task<QueryData<Client>> OnClientsQueryAsync(QueryPageOptions options)
        {
            IEnumerable<Client> items = new List<Client> {
                new() { ClientId=1,ClientName="测试一" },
                new() { ClientId=2,ClientName="测试二" },
                new() { ClientId=3,ClientName="测试三" },
                new() { ClientId=4,ClientName="测试四" },
                new() { ClientId=5,ClientName="测试五" },
            };

            if (!string.IsNullOrEmpty(options.SortName))
            {
                items = items.Sort(options.SortName, options.SortOrder);
            }

            var count = items.Count();
            if (options.IsPage)
            {
                items = items.Skip((options.PageIndex - 1) * options.PageItems).Take(options.PageItems);
            }

            return Task.FromResult(new QueryData<Client>()
            {
                Items = items.ToList(),
                TotalCount = count,
                IsAdvanceSearch = true,
                IsFiltered = true,
                IsSearch = true,
                IsSorted = true
            });
        }
    }
}
