﻿namespace BootstrapBlazorApp.Server.Data
{
    public class Client
    {
        public int ClientId { get; set; }
        public string? ClientName { get; set; }
    }
}
